#
# Cookbook Name:: hashi_vault_packages
# Recipe:: default
#
# Copyright (c) 2015 Drew Blessing, All Rights Reserved.

chef_gem 'fpm' do
  compile_time true
end

chef_gem 'package_cloud' do
  compile_time true
end

remote_file "#{Chef::Config[:file_cache_path]}/vault.zip" do
  source node['hashi_vault_packages']['zip_url']
end

package 'unzip'

bash 'unzip_and_move' do
  user 'root'
  cwd Chef::Config[:file_cache_path]
  code <<-EOH
unzip vault*
mkdir vault_project
mv vault vault_project
  EOH
end

case node['platform_family']
when 'rhel'
  package 'rpm-build'

  bash 'build_package' do
    user 'root'
    cwd "#{Chef::Config[:file_cache_path]}/vault_project"
    code <<-EOH
/opt/chef/embedded/bin/fpm -t rpm -s dir --name hashicorp_vault \
  --provides vault --prefix /usr/bin --version #{node['hashi_vault_packages']['version']} \
  --iteration #{node['hashi_vault_packages']['iteration']} .

PACKAGECLOUD_TOKEN=#{node['hashi_vault_packages']['package_cloud_token']} \
  /opt/chef/embedded/bin/package_cloud push blessing_io/hashi_vault/el/6 \
  hashicorp_vault-#{node['hashi_vault_packages']['version']}-#{node['hashi_vault_packages']['iteration']}.x86_64.rpm
    EOH
  end
when 'debian'
  bash 'build_package' do
    user 'root'
    cwd "#{Chef::Config[:file_cache_path]}/vault_project"
    code <<-EOH
/opt/chef/embedded/bin/fpm -t deb -s dir --name hashicorp_vault \
  --provides vault --prefix /usr/bin --version #{node['hashi_vault_packages']['version']} \
  --iteration #{node['hashi_vault_packages']['iteration']} .

PACKAGECLOUD_TOKEN=#{node['hashi_vault_packages']['package_cloud_token']} \
  /opt/chef/embedded/bin/package_cloud push blessing_io/hashi_vault/debian/wheezy \
  hashicorp-vault_#{node['hashi_vault_packages']['version']}-#{node['hashi_vault_packages']['iteration']}_amd64.deb
    EOH
  end
end

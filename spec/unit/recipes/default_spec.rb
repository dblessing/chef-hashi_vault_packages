#
# Cookbook Name:: hashi_vault_packages
# Spec:: default
#
# Copyright (c) 2015 Drew Blessing, All Rights Reserved.

require 'spec_helper'

describe 'hashi_vault_packages::default' do
  context 'with default attributes' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new(platform: 'centos', version: '6.5')
        .converge(described_recipe)
    end
    subject { chef_run }

    # Add tests here
  end
end

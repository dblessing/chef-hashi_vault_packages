default['hashi_vault_packages']['zip_url'] = nil
default['hashi_vault_packages']['version'] = '0.1.2'
default['hashi_vault_packages']['iteration'] = '1'
default['hashi_vault_packages']['package_cloud_token'] = nil
